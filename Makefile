REGISTRY := quay.io
NAMESPACE := lukebond
CONTAINER_NAME := postgres-operator
VERSION := workshop-03
CONTAINER_TAG = ${REGISTRY}/${NAMESPACE}/${CONTAINER_NAME}:$(VERSION)
GITREF  = $(shell git show --oneline -s | head -n 1 | awk '{print $$1}')

.PHONY: all build

all: build run

build:
	CGO_ENABLED=0 go build -o bin/postgres-operator cmd/operator/main.go
	docker build -t $(CONTAINER_TAG) --build-arg BUILDDATE=`date -u +%Y-%m-%dT%H:%M:%SZ` --build-arg VERSION=$(VERSION) --build-arg VCSREF=$(GITREF) .

clean:
	docker rmi $(CONTAINER_TAG)

run:
	docker run --net=host quay.io/lukebond/postgres-operator:v0.0.1 --master=http://127.0.0.1:8080
