package main

import (
	"flag"
	"fmt"
	"net/url"
	"os"

	"gitlab.com/lukebond/postgres-operator/pkg/controller"

	"k8s.io/client-go/1.5/kubernetes"
	"k8s.io/client-go/1.5/rest"
)

const version = "0.0.1"

var (
	caFile       string
	certFile     string
	keyFile      string
	masterHost   string
	namespace    string
	printVersion bool
	tlsInsecure  bool
)

// parse command-line args and initialise config
func init() {
	flag.StringVar(&masterHost, "master", "", "API Server addr, e.g. ' - NOT RECOMMENDED FOR PRODUCTION - http://127.0.0.1:8080'. Omit parameter to run in on-cluster mode and utilize the service account token.")
	flag.StringVar(&certFile, "cert-file", "", " - NOT RECOMMENDED FOR PRODUCTION - Path to public TLS certificate file.")
	flag.StringVar(&keyFile, "key-file", "", "- NOT RECOMMENDED FOR PRODUCTION - Path to private TLS certificate file.")
	flag.StringVar(&caFile, "ca-file", "", "- NOT RECOMMENDED FOR PRODUCTION - Path to TLS CA file.")
	flag.BoolVar(&printVersion, "version", false, "Show version and quit")
	flag.BoolVar(&tlsInsecure, "tls-insecure", false, "- NOT RECOMMENDED FOR PRODUCTION - Don't verify API server's CA certificate.")
	flag.Parse()

	namespace = os.Getenv("MY_POD_NAMESPACE")
	if len(namespace) == 0 {
		namespace = "default"
	}
}

// create controller from initialised config
func main() {
	if printVersion {
		fmt.Println("postgres-operator", version)
		os.Exit(0)
	}

	cfg, err := newControllerConfig()
	if err != nil {
		panic(err)
	}
	c := controller.New(cfg)
	c.Run()
}

// create configuration object that will be passed to the new controller
func newControllerConfig() (controller.Config, error) {
	tlsConfig := rest.TLSClientConfig{
		CertFile: certFile,
		KeyFile:  keyFile,
		CAFile:   caFile,
	}
	kubecli := createKubeCliClient(masterHost, tlsInsecure, &tlsConfig)
	cfg := controller.Config{
		MasterHost: masterHost,
		Namespace:  namespace,
		KubeCli:    kubecli,
	}
	if len(cfg.MasterHost) == 0 {
		clusterCfg, err := rest.InClusterConfig()
		if err != nil {
			return cfg, err
		}
		cfg.MasterHost = clusterCfg.Host
	}
	return cfg, nil
}

// take TLS and other configuration and create kube cli client object
func createKubeCliClient(host string, tlsInsecure bool, tlsConfig *rest.TLSClientConfig) kubernetes.Interface {
	var cfg *rest.Config
	if len(host) == 0 {
		var err error
		cfg, err = rest.InClusterConfig()
		if err != nil {
			panic(err)
		}
	} else {
		cfg = &rest.Config{
			Host:  host,
			QPS:   100,
			Burst: 100,
		}
		hostUrl, err := url.Parse(host)
		if err != nil {
			panic(fmt.Sprintf("error parsing host url %s : %v", host, err))
		}
		if hostUrl.Scheme == "https" {
			cfg.TLSClientConfig = *tlsConfig
			cfg.Insecure = tlsInsecure
		}
	}
	c, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		panic(err)
	}
	return c
}
