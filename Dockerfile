FROM golang:1.7.4-alpine

RUN apk add --no-cache ca-certificates

ADD bin/postgres-operator /usr/local/bin/postgres-operator

ENTRYPOINT [ "/usr/local/bin/postgres-operator" ]

# Container Labels
ARG BUILDDATE
ARG VCSREF
ARG VERSION

ENV BUILDDATE ${BUILDDATE}
ENV VCSREF ${VCSREF}
ENV VERSION ${VERSION}

LABEL \
  org.label-schema.name="postgres-operator" \
  org.label-schema.description="Automated PostgreSQL Kubernetes operator" \
  org.label-schema.vendor="Luke Bond" \
  org.label-schema.url="https://gitlab.com/lukebond/postgres-operator" \
  org.label-schema.usage="https://gitlab.com/lukebond/postgres-operator/README.md" \
  org.label-schema.vcs-url="https://gitlab.com/lukebond/postgres-operator" \
  org.label-schema.vcs-ref="${VCSREF}" \
  org.label-schema.build-date="${BUILDDATE}" \
  org.label-schema.version="${VERSION}" \
  org.label-schema.license="https://www.apache.org/licenses/LICENSE-2.0" \
  org.label-schema.docker.schema-version="1.0" \
  org.label-schema.docker.cmd="docker run --rm --name postgres-operator"
