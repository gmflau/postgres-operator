package spec

import (
	"time"

	"k8s.io/client-go/1.5/pkg/api/unversioned"
	"k8s.io/client-go/1.5/pkg/api/v1"
)

type PostgresCluster struct {
	unversioned.TypeMeta `json:",inline"`
	v1.ObjectMeta        `json:"metadata,omitempty"`
	Spec                 *ClusterSpec   `json:"spec"`
	Status               *ClusterStatus `json:"status"`
}

type ClusterSpec struct {
	Size         int               `json:"size"`
	Version      string            `json:"version"`
	Paused       bool              `json:"paused,omitempty"`
	NodeSelector map[string]string `json:"nodeSelector,omitempty"`
	AntiAffinity bool              `json:"antiAffinity"`
}

type ClusterPhase string

const (
	ClusterPhaseNone     ClusterPhase = ""
	ClusterPhaseCreating              = "Creating"
	ClusterPhaseRunning               = "Running"
	ClusterPhaseFailed                = "Failed"
)

type ClusterCondition struct {
	Type           ClusterConditionType `json:"type"`
	Reason         string               `json:"reason"`
	TransitionTime time.Time            `json:"transitionTime"`
}

type ClusterConditionType string

const (
	ClusterConditionReady              = "Ready"
	ClusterConditionRemovingDeadMember = "RemovingDeadMember"
	ClusterConditionRecovering         = "Recovering"
	ClusterConditionScalingUp          = "ScalingUp"
	ClusterConditionScalingDown        = "ScalingDown"
	ClusterConditionUpgrading          = "Upgrading"
)

type ClusterStatus struct {
	Phase          ClusterPhase       `json:"phase"`
	Reason         string             `json:"reason"`
	ControlPaused  bool               `json:"controlPaused"`
	Conditions     []ClusterCondition `json:"conditions"`
	Size           int                `json:"size"`
	CurrentVersion string             `json:"currentVersion"`
	TargetVersion  string             `json:"targetVersion"`
}

func (cs *ClusterStatus) IsFailed() bool {
	if cs == nil {
		return false
	}
	return cs.Phase == ClusterPhaseFailed
}

func (cs *ClusterStatus) SetPhase(p ClusterPhase) {
	cs.Phase = p
}

func (cs *ClusterStatus) PauseControl() {
	cs.ControlPaused = true
}

func (cs *ClusterStatus) Control() {
	cs.ControlPaused = false
}

func (cs *ClusterStatus) SetReason(r string) {
	cs.Reason = r
}
