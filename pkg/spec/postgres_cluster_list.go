package spec

import "k8s.io/client-go/1.5/pkg/api/unversioned"

type PostgresClusterList struct {
	unversioned.TypeMeta `json:",inline"`
	unversioned.ListMeta `json:"metadata,omitempty"`
	Items                []PostgresCluster `json:"items"`
}
