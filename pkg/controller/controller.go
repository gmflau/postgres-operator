package controller

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"sync"
	"time"

	"gitlab.com/lukebond/postgres-operator/pkg/cluster"
	"gitlab.com/lukebond/postgres-operator/pkg/spec"

	"k8s.io/client-go/1.5/kubernetes"
	apierrors "k8s.io/client-go/1.5/pkg/api/errors"
	"k8s.io/client-go/1.5/pkg/api/unversioned"
	"k8s.io/client-go/1.5/pkg/api/v1"
	v1beta1extensions "k8s.io/client-go/1.5/pkg/apis/extensions/v1beta1"
	"k8s.io/client-go/1.5/rest"
)

// this creates a group "lukeb0nd.com" with a resource "postgresclusters"
// at version "v1" (for the latter see below)
const (
	defaultVersion = "9.6.3"
	tprName        = "postgres-cluster.lukeb0nd.com"
)

var (
	ErrVersionOutdated = errors.New("Requested version is outdated in apiserver")
	initRetryWaitTime  = 30 * time.Second
)

type rawEvent struct {
	Type   string
	Object json.RawMessage
}

type Event struct {
	Type   string
	Object *spec.PostgresCluster
}

type Controller struct {
	Config
	clusters    map[string]*cluster.Cluster
	stopChMap   map[string]chan struct{}
	waitCluster sync.WaitGroup
}

type Config struct {
	MasterHost string
	Namespace  string
	KubeCli    kubernetes.Interface
}

func New(cfg Config) *Controller {
	return &Controller{
		Config:    cfg,
		clusters:  make(map[string]*cluster.Cluster),
		stopChMap: map[string]chan struct{}{},
	}
}

func (c *Controller) Run() error {
	var (
		watchVersion string
		err          error
	)

	for {
		watchVersion, err = c.initResource()
		if err == nil {
			break
		}
		fmt.Fprintln(os.Stderr, "Initialisation failed: %v", err)
		fmt.Println("Retry in %v...", initRetryWaitTime)
		time.Sleep(initRetryWaitTime)
	}

	fmt.Println("Starting at watchVersion %v", watchVersion)

	defer func() {
		for _, stopC := range c.stopChMap {
			close(stopC)
		}
		c.waitCluster.Wait()
	}()

	eventCh, errCh := c.monitor(watchVersion)

	go func() {
		for event := range eventCh {
			clus := event.Object

			if s := clus.Spec; len(s.Version) == 0 {
				s.Version = defaultVersion
			}

			fmt.Println(event)
			switch event.Type {
			case "ADDED":
				stopC := make(chan struct{})
				nc := cluster.New(c.makeClusterConfig(), clus, stopC, &c.waitCluster)
				c.stopChMap[clus.Name] = stopC
				c.clusters[clus.Name] = nc
			case "MODIFIED":
				c.clusters[clus.Name].Update(clus)
			case "DELETED":
				c.clusters[clus.Name].Delete()
				delete(c.clusters, clus.Name)
			}
		}
	}()

	return <-errCh
}

func (c *Controller) findAllClusters() (string, error) {
	fmt.Println("Finding existing clusters...")
	clusterList, err := c.listPostgresCluster(c.KubeCli.Core().GetRESTClient())
	if err != nil {
		return "", err
	}

	for i := range clusterList.Items {
		clus := clusterList.Items[i]

		if clus.Status.IsFailed() {
			fmt.Println("Ignoring failed cluster %s", clus.Name)
			continue
		}

		if s := clus.Spec; len(s.Version) == 0 {
			// TODO: set version in spec in apiserver
			s.Version = defaultVersion
		}

		stopC := make(chan struct{})
		nc := cluster.New(c.makeClusterConfig(), &clus, stopC, &c.waitCluster)
		c.stopChMap[clus.Name] = stopC
		c.clusters[clus.Name] = nc
	}

	return clusterList.ResourceVersion, nil
}

func (c *Controller) makeClusterConfig() cluster.Config {
	return cluster.Config{
		MasterHost: c.MasterHost,
		KubeCli:    c.KubeCli,
	}
}

func (c *Controller) initResource() (string, error) {
	watchVersion := "0"
	err := c.createTPR()
	if err != nil {
		if apierrors.IsAlreadyExists(err) {
			fmt.Println("Third-party resource already exists; continuing")
			watchVersion, err := c.findAllClusters()
			if err != nil {
				fmt.Fprintln(os.Stderr, "No Postgres cluster on startup")
				return "", err
			}
			fmt.Println("Watch version: %s", watchVersion)
		} else {
			return "", fmt.Errorf("fail to create storage class: %v", err)
		}
	}
	fmt.Println("Controller initialised")
	return watchVersion, nil
}

func (c *Controller) createTPR() error {
	tpr := &v1beta1extensions.ThirdPartyResource{
		ObjectMeta: v1.ObjectMeta{
			Name: tprName,
		},
		Versions: []v1beta1extensions.APIVersion{
			{Name: "v1"},
		},
		Description: "Semi-automatic PostgreSQL cluster",
	}
	_, err := c.KubeCli.Extensions().ThirdPartyResources().Create(tpr)
	if err != nil {
		return err
	}

	// TODO wait for TPR to be ready in Etcd
	return nil
}

func (c *Controller) monitor(watchVersion string) (<-chan *Event, <-chan error) {
	eventCh := make(chan *Event)
	errCh := make(chan error, 1)
	go func() {
		defer close(eventCh)

		host := c.MasterHost
		ns := c.Namespace
		httpcli := c.KubeCli.Core().GetRESTClient().Client

		for {
			resp, err := httpcli.Get(fmt.Sprintf("%s/apis/lukeb0nd.com/v1/postgresclusters?watch=true&resourceVersion=%s", host, ns, watchVersion))
			if err != nil {
				errCh <- err
				return
			}
			if resp.StatusCode != 200 {
				resp.Body.Close()
				errCh <- errors.New("Invalid status code: " + resp.Status)
				return
			}
			decoder := json.NewDecoder(resp.Body)
			for {
				ev, st, err := pollEvent(decoder)
				if err != nil {
					if err == io.EOF { // apiserver will close stream periodically
						fmt.Println("API server closed stream")
						break
					}

					fmt.Fprintln(os.Stderr, "Received invalid event from API server: %v", err)
					errCh <- err
					return
				}

				if st != nil {
					if st.Code == http.StatusGone { // event history is outdated
						errCh <- ErrVersionOutdated // go to recovery path
						return
					}
					fmt.Fprintln(os.Stderr, "Unexpected status response from API server: %v", st.Message)
					os.Exit(1)
				}

				fmt.Println("Postgres cluster event: %v %v", ev.Type, ev.Object.Spec)

				watchVersion = ev.Object.ObjectMeta.ResourceVersion
				eventCh <- ev
			}

			resp.Body.Close()
		}
	}()

	return eventCh, errCh
}

func pollEvent(decoder *json.Decoder) (*Event, *unversioned.Status, error) {
	re := &rawEvent{}
	err := decoder.Decode(re)
	if err != nil {
		if err == io.EOF {
			return nil, nil, err
		}
		return nil, nil, fmt.Errorf("Failed to decode raw event from API server (%v)", err)
	}

	if re.Type == "ERROR" {
		status := &unversioned.Status{}
		err = json.Unmarshal(re.Object, status)
		if err != nil {
			return nil, nil, fmt.Errorf("Failed to decode (%s) into unversioned.Status (%v)", re.Object, err)
		}
		return nil, status, nil
	}

	ev := &Event{
		Type:   re.Type,
		Object: &spec.PostgresCluster{},
	}
	err = json.Unmarshal(re.Object, ev.Object)
	if err != nil {
		return nil, nil, fmt.Errorf("Failed to unmarshal PostgresCluster object from data (%s): %v", re.Object, err)
	}
	return ev, nil, nil
}

func (c *Controller) listPostgresCluster(restcli *rest.RESTClient) (*spec.PostgresClusterList, error) {
	b, err := restcli.Get().RequestURI(fmt.Sprintf("%s/apis/lukeb0nd.com/v1/postgresclusters",
		c.Config.MasterHost)).DoRaw()
	if err != nil {
		return nil, err
	}
	clusters := &spec.PostgresClusterList{}
	if err := json.Unmarshal(b, clusters); err != nil {
		return nil, err
	}
	return clusters, nil
}
