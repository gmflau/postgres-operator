package cluster

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"reflect"
	"sync"
	"time"

	"k8s.io/client-go/1.5/kubernetes"
	"k8s.io/client-go/1.5/pkg/api"
	"k8s.io/client-go/1.5/pkg/api/v1"
	"k8s.io/client-go/1.5/pkg/labels"
	"k8s.io/client-go/1.5/rest"

	"gitlab.com/lukebond/postgres-operator/pkg/spec"
)

var reconcileInterval = 8 * time.Second

type clusterEventType string

const (
	eventDeleteCluster clusterEventType = "Delete"
	eventModifyCluster clusterEventType = "Modify"
)

type clusterEvent struct {
	typ     clusterEventType
	cluster *spec.PostgresCluster
}

type Config struct {
	MasterHost string
	KubeCli    kubernetes.Interface
}

type Cluster struct {
	config Config

	cluster *spec.PostgresCluster

	// in memory state of the cluster
	// status is the source of truth after Cluster struct is materialized.
	status        *spec.ClusterStatus
	memberCounter int

	eventCh chan *clusterEvent
	stopCh  chan struct{}
}

func New(config Config, p *spec.PostgresCluster, stopC <-chan struct{}, wg *sync.WaitGroup) *Cluster {
	c := &Cluster{
		config:  config,
		cluster: p,
		eventCh: make(chan *clusterEvent, 100),
		stopCh:  make(chan struct{}),
		status:  p.Status,
	}

	if c.status == nil {
		c.status = &spec.ClusterStatus{}
	}

	wg.Add(1)
	go func() {
		defer wg.Done()

		err := c.setup()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Cluster failed to setup: %v", err)
			if c.status.Phase != spec.ClusterPhaseFailed {
				c.status.SetReason(err.Error())
				c.status.SetPhase(spec.ClusterPhaseFailed)
				if err := c.updateStatus(); err != nil {
					fmt.Fprintf(os.Stderr, "Failed to update cluster phase (%v): %v", spec.ClusterPhaseFailed, err)
				}
			}
			return
		}
		c.run(stopC)
	}()

	return c
}

func (c *Cluster) setup() error {
	var shouldCreateCluster bool
	switch c.status.Phase {
	case spec.ClusterPhaseNone:
		shouldCreateCluster = true
	case spec.ClusterPhaseCreating:
		return errors.New("Cluster failed to be created")
	case spec.ClusterPhaseRunning:
		shouldCreateCluster = false

	default:
		return fmt.Errorf("Unexpected cluster phase: %s", c.status.Phase)
	}

	if shouldCreateCluster {
		return c.create()
	}
	return nil
}

func (c *Cluster) create() error {
	fmt.Println("creating cluster with Spec (%#v), Status (%#v)", c.cluster.Spec, c.cluster.Status)
	c.status.SetPhase(spec.ClusterPhaseCreating)
	if err := c.updateStatus(); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to update cluster phase (%v): %v", spec.ClusterPhaseCreating, err)
	}
	fmt.Println("PLACEHOLDER: Launch seed Postgres node!")
	return nil
}

func (c *Cluster) Delete() {
	c.send(&clusterEvent{typ: eventDeleteCluster})
}

func (c *Cluster) send(ev *clusterEvent) {
	select {
	case c.eventCh <- ev:
	case <-c.stopCh:
	default:
		panic("TODO: too many events queued...")
	}
}

func (c *Cluster) run(stopC <-chan struct{}) {
	clusterFailed := true

	defer func() {
		if clusterFailed {
			if err := c.updateStatus(); err != nil {
				fmt.Fprintf(os.Stderr, "Failed to update TPR status: %v", err)
			}
			fmt.Println("Deleting the failed cluster")
		}
		close(c.stopCh)
	}()

	c.status.SetPhase(spec.ClusterPhaseRunning)
	if err := c.updateStatus(); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to update TPR status: %v", err)
	}

	var rerr error
	for {
		select {
		case <-stopC:
			return
		case event := <-c.eventCh:
			switch event.typ {
			case eventModifyCluster:
				if isSpecEqual(event.cluster.Spec, c.cluster.Spec) {
					break
				}
				fmt.Println("Spec update: from: %v to: %v", c.cluster.Spec, event.cluster.Spec)
				c.cluster = event.cluster
			case eventDeleteCluster:
				fmt.Println("Cluster is deleted by the user")
				clusterFailed = true
				return
			}
		case <-time.After(reconcileInterval):
			if c.cluster.Spec.Paused {
				c.status.PauseControl()
				fmt.Println("Control is paused, skipping reconcilation")
				continue
			} else {
				c.status.Control()
			}

			running, pending, err := c.pollPods()
			fmt.Println("Running: %v, pending: %v", running, pending)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Fail to poll pods: %v", err)
				continue
			}
			if len(pending) > 0 {
				fmt.Println("Skip reconciliation: running (%v), pending (%v)", getPodNames(running), getPodNames(pending))
				continue
			}
			if len(running) == 0 {
				fmt.Println("All Postgres pods are dead. Trying to recover from a previous backup")
				rerr = c.disasterRecovery()
				if rerr != nil {
					fmt.Fprintf(os.Stderr, "Fail to do disaster recovery: %v", rerr)
				}
				// On normal recovery case, we need backoff. On error case, this could be either backoff or leading to cluster delete.
				break
			}

			rerr = c.reconcile(running)
			if rerr != nil {
				fmt.Fprintf(os.Stderr, "Failed to reconcile: %v", rerr)
				break
			}

			// TODO update status in Etcd
		}
	}
}

func isSpecEqual(s1, s2 *spec.ClusterSpec) bool {
	if s1.Size != s2.Size || s1.Paused != s2.Paused || s1.Version != s2.Version {
		return false
	}
	return true
}

func (c *Cluster) Update(p *spec.PostgresCluster) {
	c.send(&clusterEvent{
		typ:     eventModifyCluster,
		cluster: p,
	})
}

func (c *Cluster) pollPods() ([]*v1.Pod, []*v1.Pod, error) {
	podList, err := c.config.KubeCli.Core().Pods(c.cluster.Namespace).List(clusterListOpt(c.cluster.Name))
	if err != nil {
		return nil, nil, fmt.Errorf("Failed to list running pods: %v", err)
	}

	var running []*v1.Pod
	var pending []*v1.Pod
	for i := range podList.Items {
		pod := &podList.Items[i]
		if len(pod.OwnerReferences) < 1 {
			fmt.Println("pollPods: ignore pod %v: no owner", pod.Name)
			continue
		}
		if pod.OwnerReferences[0].UID != c.cluster.UID {
			fmt.Println("pollPods: ignore pod %v: owner (%v) is not %v", pod.Name, pod.OwnerReferences[0].UID, c.cluster.UID)
			continue
		}
		switch pod.Status.Phase {
		case v1.PodRunning:
			running = append(running, pod)
		case v1.PodPending:
			pending = append(pending, pod)
		}
	}

	return running, pending, nil
}

func (c *Cluster) updateStatus() error {
	if reflect.DeepEqual(c.cluster.Status, c.status) {
		return nil
	}

	newCluster := c.cluster
	newCluster.Status = c.status
	newCluster, err := updateClusterTPRObject(c.config.KubeCli.Core().GetRESTClient(), newCluster)
	if err != nil {
		return err
	}

	c.cluster = newCluster
	return nil
}

func (c *Cluster) reconcile(pods []*v1.Pod) error {
	fmt.Println("PLACEHOLDER: reconcile cluster with desired state!")
	return nil
}

func (c *Cluster) disasterRecovery() error {
	fmt.Println("PLACEHOLDER: disaster recovery!")
	return nil
}

func getPodNames(pods []*v1.Pod) []string {
	res := []string{}
	for _, p := range pods {
		res = append(res, p.Name)
	}
	return res
}

func clusterListOpt(clusterName string) api.ListOptions {
	return api.ListOptions{
		LabelSelector: labels.SelectorFromSet(map[string]string{
			"etcd_cluster": clusterName,
			"app":          "postgres",
		}),
	}
}

func updateClusterTPRObject(restcli *rest.RESTClient, p *spec.PostgresCluster) (*spec.PostgresCluster, error) {
	if len(p.ResourceVersion) == 0 {
		return nil, errors.New("Resource version is not provided")
	}
	uri := fmt.Sprintf("/apis/lukeb0nd.com/v1/postgresclusters/%s", p.Name)
	b, err := restcli.Put().RequestURI(uri).Body(p).DoRaw()
	if err != nil {
		return nil, err
	}
	return readOutCluster(b)
}

func readOutCluster(b []byte) (*spec.PostgresCluster, error) {
	cluster := &spec.PostgresCluster{}
	if err := json.Unmarshal(b, cluster); err != nil {
		return nil, err
	}
	return cluster, nil
}
