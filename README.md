# PostgreSQL Kubernetes Operator

## Building

```
$ make build
```

This will build the Golang code and then build a container image with Docker.

## Running with Minikube

Ensure minikube is running.
Launch the Kubernetes API Proxy locally:

```
$ kubectl proxy --port=8080
```

Run the operator locally, pointing to the Minikube cluster, either with Docker:

```
$ docker run --net=host quay.io/lukebond/postgres-operator:v0.0.1 --master=http://127.0.0.1:8080
```

...or just run the binary directly:

```
./bin/postgres-operator
```
